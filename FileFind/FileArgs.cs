﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileFind
{
    public class FileArgs : EventArgs
    {        
        public string FileName { get; set; }
        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
       
    }
}
