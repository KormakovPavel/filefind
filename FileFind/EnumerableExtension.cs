﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileFind
{
    public static class EnumerableExtension
    {
        public static T GetMax<T>(this IEnumerable<T> self, Func<T, float> func) where T : class
        {
            T MaxElement = null;
            foreach (var item in self)
            {
                if (func(item) > func(MaxElement))
                {
                    MaxElement = item;
                }
            }
            return MaxElement;
        }
    }
}
