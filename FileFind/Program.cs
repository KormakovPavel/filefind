﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace FileFind
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите каталога для поиска: ");
            string inputPath = Console.ReadLine();
            Console.Write("Введите время сканирования каталога в милисекундах: ");
            int.TryParse(Console.ReadLine(), out int timeOut);

            DirectoryScanner directoryScan = new DirectoryScanner(inputPath, timeOut);            
            directoryScan.Scan();

            Console.WriteLine($"Всего просканировано файлов: {directoryScan.CurrentFileCount} из {directoryScan.MaxFileCount}");
            Console.WriteLine($"Файл с наибольшим размером: {directoryScan.GetFileNameForMaxFileSize}");
            Console.ReadLine();
        }        
    }
}
