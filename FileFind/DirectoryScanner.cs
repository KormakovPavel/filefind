﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Threading;

namespace FileFind
{
    class DirectoryScanner 
    {
        private readonly string _inputPath;
        private readonly int _timeOut;
        private int _maxFileCount;
        private readonly List<FileInfo> _fileInfos;
        public event EventHandler<FileArgs> FileFound;

        public DirectoryScanner(string inputPath, int timeOut)
        {
            _inputPath = inputPath;
            _timeOut = timeOut;
            _maxFileCount = default;
            _fileInfos = new List<FileInfo>();
            FileFound += Message.Show;
        }

        public int CurrentFileCount
        {
            get
            {
                return _fileInfos?.Count ?? default;
            }
        }

        public int MaxFileCount
        {
            get
            {
                return _maxFileCount;
            }
        }

        public string GetFileNameForMaxFileSize
        {
            get
            {
                return _fileInfos.GetMax(GetFileSize)?.Name ?? "список пуст";
            }
        }

        private float GetFileSize(FileInfo fileInfo)
        {
            return fileInfo?.Length ?? default;
        }

        public void Scan()
        {
            using CancellationTokenSource token = new CancellationTokenSource();
            token.CancelAfter(_timeOut);

            if (Directory.Exists(_inputPath))
            {
                var files = Directory.GetFiles(_inputPath);
                _maxFileCount = files.Length;
                foreach (var fileName in files)
                {
                    if (token.IsCancellationRequested)
                    {
                        FileFound -= Message.Show;
                        break;
                    }
                    FileFound?.Invoke(this, new FileArgs(fileName));
                    _fileInfos.Add(new FileInfo(fileName));
                }
            }
            else
            {
                FileFound?.Invoke(this, new FileArgs("не найдены файлы, каталог не существует"));
            }
        }
    }
}
