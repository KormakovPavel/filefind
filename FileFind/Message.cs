﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileFind
{
    public static class Message
    {
        public static void Show(object sender, FileArgs e)
        {
            Console.WriteLine($"Файл: {e.FileName}");
        }
    }
}
